#include <iostream>
#include <stack>

using namespace std;

int main()
{
	stack <int> FirstNum;
	stack <int> SecondNum;
	stack <int> resultStack;
	stack <int> Hold;
	stack <int> showFirstNum;
	stack <int> showSecondNum;
	int minusSign = 0;
	int size;
	int result;
	int hold;
	int check;
	int number;
	
	//List No.1
	int A[19] = {1,3,4,4,8,2,3,5,0,8,9,7,4,5,3,4,5,2,3};
	cout << "First numbers : ";
	for(int i =0;i<19;i++){
		cout << A[i];
		FirstNum.push(A[i]);
	}
	cout << endl;
	Hold = FirstNum;
	//List No.2
	int B[17] = {2,3,4,7,2,3,4,7,0,9,4,7,3,0,4,7,5};
	cout << "Second numbers : ";
	for(int i =0 ;i<17;i++)
	{
			cout << B[i];
			SecondNum.push(B[i]);
	}
	cout << endl;
	//Type of calculate
	cout << "\nInput what type of your calculate (1 = Plus , 2 = Minus) : ";
	cin >> check;
	while(check != 1 && check != 2)
	{
		cout << "Wrong in put ,Try again! : ";
		cin >> check;
	}
	
	//Plus
	if(check == 1)
	{
		if(SecondNum.size() > FirstNum.size()) //If size No.2 > No.1
		{
			//Swap stack because The program make for size No.1 > No.2 only
			FirstNum = SecondNum; 
			SecondNum = Hold;
		}
		while(!FirstNum.empty() && !SecondNum.empty())
		{
			result = FirstNum.top() + SecondNum.top(); //Sum on the top stack
			FirstNum.pop(); //Remove top No.1
			SecondNum.pop(); //Remove top No.2
			if(FirstNum.size() > 0)
			{
				if(result >= 10) //If sum more than or equal 10
				{
					hold = result % 10; //Check remain after divide by ten
					resultStack.push(hold); //Push that remain to stack
					FirstNum.top() = FirstNum.top() + 1; //Make next top after remove top No.1 plus 1
				}

				if(result < 10) //If sum less than 10
				{
					hold = result % 10; //Check remain after divide by ten
					resultStack.push(hold); //Push that remain to stack
				}
			}
			else 
				resultStack.push(result);	
		}

		if(!FirstNum.empty()) //If the stack No.2 is empty make push remain from stack No.1
		{
			while(!FirstNum.empty())
			{
				resultStack.push(FirstNum.top());
				FirstNum.pop();
			}
		}
	}
	
	//Minus		
	if(check == 2)	
	{
	
		 if(SecondNum.size() > FirstNum.size()) //If size No.2 > No.1
		{
			//Swap stack because The program make for size No.1 > No.2 only
			minusSign = 1; 
			FirstNum = SecondNum;
			SecondNum = Hold;
		}
		 else if (SecondNum > FirstNum && SecondNum.size() == FirstNum.size()) //If value of No.2 more than No.1 and size is same
		 {
		 	 //Swap stack because The program make for size No.1 > No.2 only
			 minusSign = 1;
			 FirstNum = SecondNum;
			 SecondNum = Hold;
		 }
		 else if (FirstNum.size() > SecondNum.size()) //If size No.1 > No.2
		 {
		 	 //No swap
			 FirstNum = FirstNum;
			 SecondNum = SecondNum;
		 }
		while(!FirstNum.empty() && !SecondNum.empty())
		{
			result = FirstNum.top() - SecondNum.top(); //minus
			FirstNum.pop(); //remove top from No.1
			SecondNum.pop(); //remove top from No.2
		if(FirstNum.size() > 0)
		{
			if(result < 0) //if result less than 0
			{
				result += 10; //plus 10 because result must bring 1 from another and make itself plus 10
				hold = result % 10; //Find the remain after divide by 10
				resultStack.push(hold); //push the remain
				FirstNum.top() = FirstNum.top() - 1; //another top minus 1
			}
			else if(result >= 0) //if result more than or equal to 10
			{
				hold = result % 10; //Find the remain after divide by 10
				resultStack.push(hold); //push the remain
			}
		}
		else 
			resultStack.push(result);	
		}

		if(!FirstNum.empty()) //if stack No.2 is empty push the remain of stack No.1
		{
			while(!FirstNum.empty())
			{
				resultStack.push(FirstNum.top());
				FirstNum.pop();
			}
		}
	}
	//Type for result
	if (check == 1)
		cout << "\nYour sum is : ";
	else
	{
		cout << "\nYour minus is : ";
		if(minusSign == 1)
		{
			cout << "-";
		}
    }
	
	while(resultStack.size() != 0)
	{
		cout << resultStack.top();
		resultStack.pop();
	}
	cout << endl;
	system("PAUSE");
	return 0;
}

